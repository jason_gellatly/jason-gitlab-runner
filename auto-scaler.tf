# Some configuration for you
locals {
  max_instances       = 5 # Maximum dask instances you want
  min_instances       = 1 # Minimum dask instances you want
  cpu_scale_threshold = 0.75 # If aggregate CPU hits this for the dask instances, it'll scale
}

# A health check for your instances, if you need it
resource "google_compute_health_check" "dask_instance_health_check" {
  name                = "dask-instance-health-check"
  check_interval_sec  = 5
  timeout_sec         = 5
  healthy_threshold   = 2
  unhealthy_threshold = 10

  # Change this to whatever health checks are appropriate for your instances
  # if any
  http_health_check {
    request_path = "/health"
    port         = "80"
  }
}

# ~Equivalent to the AWS AMI, this will use Debian 9.
# Maybe there's a dask compute image out there? Or you can build your own instead
data "google_compute_image" "debian_9" {
  family  = "debian-9"
  project = "debian-cloud"
}

# Creates a template for the MIG to use
resource "google_compute_instance_template" "dask_instance_template" {
  name           = "dask-instance-template"
  machine_type   = "n1-standard-1" # Probably needs to be something more appropriate
  can_ip_forward = false # Doubt you'll need this, so it's set to false

  # Boot off debian google_compute_image from above
  disk {
    source_image = data.google_compute_image.debian_9.self_link
  }

  # I don't know your network infra, so this will just put it in the default one
  network_interface {
    network = "default"
  }
}

resource "google_compute_target_pool" "dask_instance_target_pool" {
  name = "dask-instance-target-pool"
}

resource "google_compute_instance_group_manager" "dask_instance_group_manager" {
  name = "dask-mig"

  base_instance_name = "dask"
  zone               = "australia-southeast1-a"

  version {
    instance_template  = google_compute_instance_template.dask_instance_template.self_link
  }

  target_pools = [google_compute_target_pool.dask_instance_target_pool.self_link]

  # Remove this if you don't need/want health checks
  # Commented out for now because the instances will keep failing this health check
  # auto_healing_policies {
  #   health_check      = google_compute_health_check.dask_instance_health_check.self_link
  #   initial_delay_sec = 300
  # }
}


resource "google_compute_autoscaler" "dask-instance-autoscaler" {
  name   = "dask-instance-autoscaler"
  zone   = "australia-southeast1-a"
  target = google_compute_instance_group_manager.dask_instance_group_manager.id

  # You can scale off all kinds of stuff here. You can export custom metrics even
  # https://www.terraform.io/docs/providers/google/r/compute_autoscaler.html#autoscaling_policy
  autoscaling_policy {
    max_replicas    = local.max_instances
    min_replicas    = local.min_instances
    cooldown_period = 60

    cpu_utilization {
      target = local.cpu_scale_threshold
    }
  }
}
