## Gitlab runners
Prefer to (if you can) use shared gitlab runners. They're enabled by default for Gitlab.
If not, go to your repo (project), click settings, go to CI/CD and enable them there.

Drop your .gitlab-ci.yml (the one in here as an example) in to your project and it'll start work straight away.
You just need to figure out what you want it to do from there. [Ref docs here](https://docs.gitlab.com/ee/ci/yaml/README.html)

## Managed instance groups (auto scalers)
There's terraform in this repository which could create the auto scaler for you, and then you can toss the TF if you want (or keep doing TF).

Set up a Service Account in GCP with Project Owner and Compute Engine Admin privs (lazy, I know).

Grab its credentials and chuck them in 'account.json' in this directory.

Grab your project's ID (click the Projects bar at the top of the console and grab the ID) and chuck it in 'providers'. You may need to change the region if it's wrong.

Run `terraform init` to get the terraform set up.

Then just `terraform plan` to make sure everything looks OK.

`terraform apply` to create all the resources. You can change the terraform to use different images or always have more/less instances. Up to you.
